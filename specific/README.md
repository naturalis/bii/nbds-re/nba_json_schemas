# Additional schema's
These schema's can be used to meet exceptional requirements for specific data suppliers. They are merged with the relevant basic schema at runtime, using jq:

```bash
  jq -s '.[0] * .[1]' general_schema_file.json specific_schema_file.json 
```

## Behaviour array property values
Please be aware that the jq-operation **merges** each document's **properties**, but **overwrites array-values** in the general schema with those in the additional schema. If for instance, merging

```javascript
{
  "scientificName": {
    "type": "object",
    "properties": {
      "fullScientificName": {
        "type": "string"
      }
    },
    "required": [
      "scientificName"
    ]
  }
}
```

with
```javascript
{
  "scientificName": {
    "type": "object",
    "properties": {
      "scientificNameGroup": {
        "type": "string"
      }
    },
    "required": [
      "scientificNameGroup"
    ]
  }
}
```

will result in 
```javascript
{
  "scientificName": {
    "type": "object",
    "properties": {
      "fullScientificName": {
        "type": "string"
      },
      "scientificNameGroup": {
        "type": "string"
      }
    },
    "required": [
      "scientificNameGroup"
    ]
  }
}
```

in which the original values in the required-array have been lost. To avoid, always also include the original values in the specific schema:
```javascript
{
  "scientificName": {
    "type": "object",
    "properties": {
      "scientificNameGroup": {
        "type": "string"
      }
    },
    "required": [
      "scientificName",
      "scientificNameGroup"
    ]
  }
}
```
