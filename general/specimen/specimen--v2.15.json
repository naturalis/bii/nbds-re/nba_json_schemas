{
  "$id": "http://api.biodiversitydata.nl/v2/specimen/",
  "version": "2.15",
  "type": "object",
  "properties": {
    "sourceSystem": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "enum": [
            "CRS",
            "BRAHMS",
            "XENO-CANTO",
            "WAARNEMING.NL"
          ]
        },
        "name": {
          "type": "string",
          "enum": [
            "Naturalis - Botany catalogues",
            "Naturalis - Zoology and Geology catalogues",
            "waarneming.nl - Species observations in the Netherlands",
            "xeno-canto.org - Bird sounds"
          ]
        }
      },
      "required": [
        "code",
        "name"
      ],
      "additionalProperties": false
    },
    "sourceSystemId": {
      "type": "string"
    },
    "recordURI": {
      "type": "string",
      "format": "uri"
    },
    "id": {
      "type": "string"
    },
    "unitID": {
      "type": "string"
    },
    "unitGUID": {
      "type": "string",
      "format": "uri"
    },
    "collectorsFieldNumber": {
      "type": "string"
    },
    "assemblageID": {
      "type": "string"
    },
    "sourceInstitutionID": {
      "type": "string",
      "enum": [
        "Naturalis Biodiversity Center",
        "Stichting Observation International​",
        "Stichting Xeno-canto voor Natuurgeluiden"
      ]
    },
    "sourceID": {
      "type": "string"
    },
    "owner": {
      "type": "string",
      "enum": [
        "Naturalis Biodiversity Center",
        "Stichting Observation International​",
        "Stichting Xeno-canto voor Natuurgeluiden"
      ]
    },
    "licenseType": {
      "type": "string",
      "enum": [
        "Copyright"
      ]
    },
    "license": {
      "type": "string",
      "enum": [
        "CC0",
        "CC BY",
        "CC BY-NC"
      ]
    },
    "recordBasis": {
      "type": "string"
    },
    "kindOfUnit": {
      "type": "string"
    },
    "collectionType": {
      "type": "string"
    },
    "sex": {
      "type": "string",
      "enum": [
        "female",
        "male",
        "hermaphrodite",
        "mixed",
        "unknowable",
        "undetermined"
      ]
    },
    "phaseOrStage": {
      "type": "string"
    },
    "title": {
      "type": "string"
    },
    "notes": {
      "type": "string"
    },
    "preparationType": {
      "type": "string"
    },
    "numberOfSpecimen": {
      "type": "integer"
    },
    "fromCaptivity": {
      "type": "boolean"
    },
    "objectPublic": {
      "type": "boolean"
    },
    "multiMediaPublic": {
      "type": "boolean"
    },
    "acquiredFrom": {
      "type": "object",
      "properties": {
        "agentText": {
          "type": "string"
        }
      },
      "additionalProperties": false
    },
    "gatheringEvent": {
      "type": "object",
      "properties": {
        "projectTitle": {
          "type": "string"
        },
        "worldRegion": {
          "type": "string"
        },
        "continent": {
          "type": "string"
        },
        "country": {
          "type": "string"
        },
        "iso3166Code": {
          "type": "string"
        },
        "provinceState": {
          "type": "string"
        },
        "island": {
          "type": "string"
        },
        "locality": {
          "type": "string"
        },
        "city": {
          "type": "string"
        },
        "sublocality": {
          "type": "string"
        },
        "localityText": {
          "type": "string"
        },
        "dateTimeBegin": {
          "type": "string",
          "format": "date-time"
        },
        "dateTimeEnd": {
          "type": "string",
          "format": "date-time"
        },
        "method": {
          "type": "string"
        },
        "altitude": {
          "type": "string"
        },
        "altitudeUnifOfMeasurement": {
          "type": "string"
        },
        "depth": {
          "type": "string"
        },
        "depthUnitOfMeasurement": {
          "type": "string"
        },
        "gatheringPersons": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "agentText": {
                "type": "string"
              },
              "fullName": {
                "type": "string"
              },
              "organization": {
                "type": "object",
                "properties": {
                  "agentText": {
                    "type": "string"
                  },
                  "name": {
                    "type": "string"
                  }
                },
                "additionalProperties": false
              }
            },
            "additionalProperties": false
          }
        },
        "gatheringOrganizations": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "agentText": {
                "type": "string"
              },
              "name": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "siteCoordinates": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "longitudeDecimal": {
                "type": "number",
                "minimum": -180,
                "maximum": 180
              },
              "latitudeDecimal": {
                "type": "number",
                "minimum": -90,
                "maximum": 90
              },
              "gridCellSystem": {
                "type": "string"
              },
              "gridLatitudeDecimal": {
                "type": "integer"
              },
              "gridLongitudeDecimal": {
                "type": "integer"
              },
              "gridCellCode": {
                "type": "string"
              },
              "gridQualifier": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "chronoStratigraphy": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "youngRegionalSubstage": {
                "type": "string"
              },
              "youngRegionalStage": {
                "type": "string"
              },
              "youngRegionalSeries": {
                "type": "string"
              },
              "youngDatingQualifier": {
                "type": "string"
              },
              "youngInternSystem": {
                "type": "string"
              },
              "youngInternSubstage": {
                "type": "string"
              },
              "youngInternStage": {
                "type": "string"
              },
              "youngInternSeries": {
                "type": "string"
              },
              "youngInternErathem": {
                "type": "string"
              },
              "youngInternEonothem": {
                "type": "string"
              },
              "youngChronoName": {
                "type": "string"
              },
              "youngCertainty": {
                "type": "string"
              },
              "oldDatingQualifier": {
                "type": "string"
              },
              "chronoPreferredFlag": {
                "type": "boolean"
              },
              "oldRegionalSubstage": {
                "type": "string"
              },
              "oldRegionalStage": {
                "type": "string"
              },
              "oldRegionalSeries": {
                "type": "string"
              },
              "oldInternSystem": {
                "type": "string"
              },
              "oldInternSubstage": {
                "type": "string"
              },
              "oldInternStage": {
                "type": "string"
              },
              "oldInternSeries": {
                "type": "string"
              },
              "oldInternErathem": {
                "type": "string"
              },
              "oldInternEonothem": {
                "type": "string"
              },
              "oldChronoName": {
                "type": "string"
              },
              "chronoIdentifier": {
                "type": "string"
              },
              "oldCertainty": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "lithoStratigraphy": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "qualifier": {
                "type": "string"
              },
              "preferredFlag": {
                "type": "boolean"
              },
              "member2": {
                "type": "string"
              },
              "member": {
                "type": "string"
              },
              "informalName2": {
                "type": "string"
              },
              "informalName": {
                "type": "string"
              },
              "importedName2": {
                "type": "string"
              },
              "importedName1": {
                "type": "string"
              },
              "lithoIdentifier": {
                "type": "string"
              },
              "formation2": {
                "type": "string"
              },
              "formationGroup2": {
                "type": "string"
              },
              "formationGroup": {
                "type": "string"
              },
              "formation": {
                "type": "string"
              },
              "certainty2": {
                "type": "string"
              },
              "certainty": {
                "type": "string"
              },
              "bed2": {
                "type": "string"
              },
              "bed": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "bioStratigraphy": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "youngBioDatingQualifier": {
                "type": "string"
              },
              "youngBioName": {
                "type": "string"
              },
              "youngFossilZone": {
                "type": "string"
              },
              "youngFossilSubZone": {
                "type": "string"
              },
              "youngBioCertainty": {
                "type": "string"
              },
              "youngStratType": {
                "type": "string"
              },
              "bioDatingQualifier": {
                "type": "string"
              },
              "bioPreferredFlag": {
                "type": "boolean"
              },
              "rangePosition": {
                "type": "string"
              },
              "oldBioName": {
                "type": "string"
              },
              "bioIdentifier": {
                "type": "string"
              },
              "oldFossilzone": {
                "type": "string"
              },
              "oldFossilSubzone": {
                "type": "string"
              },
              "oldBioCertainty": {
                "type": "string"
              },
              "oldBioStratType": {
                "type": "string"
              }
            },
            "additionalProperties": false
          }
        },
        "associatedTaxa": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "name": {
                "type": "string"
              },
              "relationType": {
                "type": "string",
                "enum": [
                  "has host",
                  "has parasite",
                  "has food plant",
                  "in relation with"
                ]
              }
            },
            "additionalProperties": false
          }
        },
        "namedAreas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "areaName": {
                "type": "string"
              },
              "areaClass": {
                "type": "string",
                "enum": [
                  "continent",
                  "county",
                  "islandGroup",
                  "municipality",
                  "waterBody"
                ]
              }
            },
            "additionalProperties": false
          }
        },
        "biotopeText": {
          "type": "string"
        }
      },
      "additionalProperties": false
    },
    "identifications": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "taxonRank": {
            "type": "string"
          },
          "scientificName": {
            "type": "object",
            "properties": {
              "fullScientificName": {
                "type": "string"
              },
              "taxonomicStatus": {
                "type": "string"
              },
              "genusOrMonomial": {
                "type": "string"
              },
              "subgenus": {
                "type": "string"
              },
              "specificEpithet": {
                "type": "string"
              },
              "infraspecificEpithet": {
                "type": "string"
              },
              "infraspecificMarker": {
                "type": "string"
              },
              "nameAddendum": {
                "type": "string"
              },
              "authorshipVerbatim": {
                "type": "string"
              },
              "author": {
                "type": "string"
              },
              "year": {
                "type": "string"
              },
              "scientificNameGroup": {
                "type": "string"
              },
              "references": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "titleCitation": {
                      "type": "string"
                    },
                    "citationDetail": {
                      "type": "string"
                    },
                    "uri": {
                      "type": "string"
                    },
                    "author": {
                      "type": "object",
                      "properties": {
                        "agentText": {
                          "type": "string"
                        },
                        "fullName": {
                          "type": "string"
                        },
                        "organization": {
                          "type": "object",
                          "properties": {
                            "agentText": {
                              "type": "string"
                            },
                            "name": {
                              "type": "string"
                            }
                          },
                          "additionalProperties": false
                        }
                      },
                      "additionalProperties": false
                    },
                    "publicationDate": {
                      "type": "string",
                      "format": "date-time"
                    }
                  },
                  "additionalProperties": false
                }
              },
              "experts": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "agentText": {
                      "type": "string"
                    },
                    "fullName": {
                      "type": "string"
                    },
                    "organization": {
                      "type": "object",
                      "properties": {
                        "agentText": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        }
                      },
                      "additionalProperties": false
                    }
                  },
                  "additionalProperties": false
                }
              }
            },
            "additionalProperties": false
          },
          "typeStatus": {
            "type": "string",
            "enum": [
              "allotype",
              "epitype",
              "holotype",
              "isoepitype",
              "isolectotype",
              "isoneotype",
              "isosyntype",
              "isotype",
              "lectotype",
              "neotype",
              "paralectotype",
              "paratype",
              "syntype",
              "topotype",
              "type",
              "hapantotype"
            ]
          },
          "dateIdentified": {
            "type": "string",
            "format": "date-time"
          },
          "defaultClassification": {
            "type": "object",
            "properties": {
              "kingdom": {
                "type": "string"
              },
              "phylum": {
                "type": "string"
              },
              "className": {
                "type": "string"
              },
              "order": {
                "type": "string"
              },
              "superFamily": {
                "type": "string"
              },
              "family": {
                "type": "string"
              },
              "genus": {
                "type": "string"
              },
              "subgenus": {
                "type": "string"
              },
              "specificEpithet": {
                "type": "string"
              },
              "infraspecificEpithet": {
                "type": "string"
              },
              "infraspecificRank": {
                "type": "string"
              }
            },
            "additionalProperties": false
          },
          "systemClassification": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "rank": {
                  "type": "string"
                },
                "name": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "vernacularNames": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "name": {
                  "type": "string"
                },
                "language": {
                  "type": "string"
                },
                "preferred": {
                  "type": "boolean"
                },
                "references": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "titleCitation": {
                        "type": "string"
                      },
                      "citationDetail": {
                        "type": "string"
                      },
                      "uri": {
                        "type": "string"
                      },
                      "author": {
                        "type": "object",
                        "properties": {
                          "agentText": {
                            "type": "string"
                          },
                          "fullName": {
                            "type": "string"
                          },
                          "organization": {
                            "type": "object",
                            "properties": {
                              "agentText": {
                                "type": "string"
                              },
                              "name": {
                                "type": "string"
                              }
                            },
                            "additionalProperties": false
                          }
                        },
                        "additionalProperties": false
                      },
                      "publicationDate": {
                        "type": "string",
                        "format": "date-time"
                      }
                    },
                    "additionalProperties": false
                  }
                },
                "experts": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "agentText": {
                        "type": "string"
                      },
                      "fullName": {
                        "type": "string"
                      },
                      "organization": {
                        "type": "object",
                        "properties": {
                          "agentText": {
                            "type": "string"
                          },
                          "name": {
                            "type": "string"
                          }
                        },
                        "additionalProperties": false
                      }
                    },
                    "additionalProperties": false
                  }
                }
              },
              "additionalProperties": false
            }
          },
          "identificationQualifiers": {
            "type": "array",
            "items": {
              "type": "string"
            }
          },
          "identifiers": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "agentText": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "taxonomicEnrichments": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "vernacularNames": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "name": {
                        "type": "string"
                      },
                      "language": {
                        "type": "string"
                      }
                    },
                    "additionalProperties": false
                  }
                },
                "synonyms": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "fullScientificName": {
                        "type": "string"
                      },
                      "taxonomicStatus": {
                        "type": "string"
                      },
                      "genusOrMonomial": {
                        "type": "string"
                      },
                      "subgenus": {
                        "type": "string"
                      },
                      "specificEpithet": {
                        "type": "string"
                      },
                      "infraspecificEpithet": {
                        "type": "string"
                      },
                      "authorshipVerbatim": {
                        "type": "string"
                      }
                    },
                    "additionalProperties": false
                  }
                },
                "sourceSystem": {
                  "type": "object",
                  "properties": {
                    "code": {
                      "type": "string"
                    }
                  },
                  "additionalProperties": false
                },
                "taxonId": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "preferred": {
            "type": "boolean"
          },
          "verificationStatus": {
            "type": "string"
          },
          "rockType": {
            "type": "string"
          },
          "associatedFossilAssemblage": {
            "type": "string"
          },
          "rockMineralUsage": {
            "type": "string"
          },
          "associatedMineralName": {
            "type": "string"
          },
          "remarks": {
            "type": "string"
          }
        },
        "additionalProperties": false
      }
    },
    "associatedMultiMediaUris": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "accessUri": {
            "type": "string",
            "format": "uri"
          },
          "format": {
            "type": "string"
          },
          "variant": {
            "type": "string"
          }
        },
        "additionalProperties": false
      }
    },
    "previousSourceID": {
      "type": "array",
      "items": {
          "type": "string"
        }
    },
    "previousUnitsText": {
      "type": "string"
    }
  },
  "required": [
    "id",
    "sourceSystem",
    "sourceSystemId",
    "unitID",
    "sourceInstitutionID",
    "sourceID",
    "licenseType",
    "license",
    "recordBasis",
    "collectionType",
    "objectPublic"
  ],
  "additionalProperties": false
}
